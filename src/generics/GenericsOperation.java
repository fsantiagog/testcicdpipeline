/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generics;

/**
 *
 * @author orion
 */
public interface GenericsOperation <T> {
    T ejecutar(T t);
    default String get(T t){
        return t.toString();
    }
}
